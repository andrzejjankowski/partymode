from django import forms


class PostUrl(forms.Form):
    url = forms.URLField(label='Your source site', required=True)
