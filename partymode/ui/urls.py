# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import patterns, include, url

from partymode.ui.views import QueueView

urlpatterns = patterns(
    'partymode.ui.views',
    url(r'^queue/', QueueView.as_view(), name='queue'),
    url(r'^post-url/', 'post_url'),
    url(r'^get_pending_resources/', 'get_pending_resources'),
    url(r'^get_next_resource/', 'get_next_resource'),
    url(r'^get_active_resource/', 'get_active_resource'),
)

