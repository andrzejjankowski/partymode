from django.shortcuts import render, redirect
# Create your views here.

from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import TemplateView

import json

from partymode import dispatcher
from partymode.core import models as core_models
from partymode.ui import forms


class BaseView(TemplateView):
    pass


class QueueView(BaseView):
    template_name = 'queue.html'

    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        context.update({

        })
        return context


def get_pending_resources(request):
    # up to 10
    if request.method == 'GET':
        resources = core_models.Resource.get_pending_resources(how_many=10)
        data_to_json = []
        for res in resources:
            data_to_json.append({
                'resource_url': res.resource_url,
                'resource_type': res.resource_type,
                'resource_title': res.resource_title,
            })
        jsoned = json.dumps(data_to_json)
        return HttpResponse(jsoned, content_type="application/json")
    return redirect('queue')


def get_next_resource(request):
    if request.method == 'GET':
        active_res = core_models.Resource.get_active_resource()
        if active_res:
            active_res.change_status(core_models.DISPLAYED)
        resources = core_models.Resource.get_pending_resources(how_many=1)
        if resources:
            resource = resources[0]
            resource.change_status(core_models.DISPLAYING)
            jsoned = resource.to_json()
        else:
            jsoned = json.dumps({})
        return HttpResponse(jsoned, content_type="application/json")
    return redirect('queue')

def get_active_resource(request):
    if request.method == 'GET':
        active_res = core_models.Resource.get_active_resource()
        if active_res:
            jsoned = active_res.to_json()
        else:
            jsoned = json.dumps(None)
        return HttpResponse(jsoned, content_type="application/json")
    return redirect('queue')


def main(request):
    return render(request, 'base.html', {
    })


def post_url(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = forms.PostUrl(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            url = form.cleaned_data['url']
            module_path, class_name = dispatcher.get_resource_processor(url)
            if module_path and class_name:
                job_id = dispatcher.run_resource_processor(
                    url, module_path, class_name
                )
                return redirect('queue')
            else:
                pass
                #info for user about not supported site
    else:
        form = forms.PostUrl()  # An unbound form

    return render(request, 'post_url.html', {
        'form': form,
        'url': '/post-url/',
    })
