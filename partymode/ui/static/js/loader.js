var renderItem = function (entry) {
    return '<li class="list-group-item">' + entry.resource_title + ' </li>';
};

function Loader () {
    this.self=this;
}

Loader.prototype.showTime = function () {
    $("#time-widget").html(new Date());
};

Loader.prototype.get_next_resource = function() {
    $.ajax("/get_next_resource", function(success, data) {

    });
}

Loader.prototype.load = function() {
    var item = '';

    $.get("/get_active_resource/", function(data) {
        if (data){
            $("#resource_title h1").html(data.resource_title);
            if (data.resource_type == 1) {
                $("#resource_content").html("<img src='" + data.resource_url + "'></img>")
            }
            else {
                $("#resource_content").html("<iframe width='748' height='460' src='http://www.youtube.com/embed/" + data.resource_url + "?rel=0'></iframe>");
            }
        }
    }).error(function () {
        var fallback = { 'url' : 'http://media.tumblr.com/5584bd8c76db38340dce35ec2114850e/tumblr_inline_mxinoj5IC11raprkq.gif',
        'title': '100 percent uptime for Black Friday and Cyber Monday'
        };

        $("#resource_content").html("<img src='" + fallback.resource_url + "'></img>");
        $("#resource_title h1").html(fallback.resource_title);
    });

    $.get("/get_pending_resources", function(data) {
        $("#queue").html('');
        var output = '';
        $.each(data, function(index, item) {
            output += renderItem(item);
        });
        $("#queue").html(output);
    }).error(function () {
        var item = {
            'resource_title': '100 percent uptime for Black Friday and Cyber Monday',
            'eta': '10 sec'
        };
        var output = '';
        for (var i=0; i < 10; i++) {
            output += renderItem(item);
        };
        $("#queue").html(output);
    });
};

var loader = new Loader();
loader.load();
setInterval(loader.load, 15000);
setInterval(loader.showTime, 15000);
setInterval(loader.get_next_resource, 15000);
