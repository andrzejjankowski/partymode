# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import patterns, include, url

from django.contrib import admin
from partymode.ui import views


admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^', include('partymode.ui.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

