# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
)
from django.db import models
from django.utils.translation import ugettext_lazy as _

import json

IMAGE = 1
VIDEO = 2

WAITING = 1
DISPLAYING = 2
DISPLAYED = 3


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError(u'E-mail address is required.')
        user = self.model(
            email=UserManager.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name=u'e-mail',
        max_length=255,
        unique=True,
        db_index=True,
    )
    firstname = models.CharField(
        verbose_name=u'first name',
        max_length=100,
        blank=True,
    )
    surname = models.CharField(
        verbose_name=u'last name',
        max_length=150,
        blank=True,
    )
    created = models.DateField(
        auto_now=False,
        auto_now_add=True,
    )
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        if self.firstname and self.surname:
            return '%s %s' % (self.firstname, self.surname)
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin


class Resource(models.Model):
    TYPES = (
        (IMAGE, _("Image")),
        (VIDEO, _("Video")),
    )
    STATUSES = (
        (WAITING, _('Waiting')),
        (DISPLAYING, _('Displaying')),
        (DISPLAYED, _('Displayed')),
    )
    user = models.ForeignKey(User, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True, auto_now_add=True)
    resource_type = models.PositiveSmallIntegerField(
        choices=TYPES,
        db_index=True,
    )
    status = models.PositiveSmallIntegerField(
        choices=STATUSES,
        db_index=True,
        default=WAITING,
    )
    base_url = models.URLField(max_length=2000)
    resource_url = models.URLField(max_length=2000)
    resource_title = models.TextField()

    def to_json(self):
        data_to_json = {
            'resource_url': self.resource_url,
            'resource_type': self.resource_type,
            'resource_title': self.resource_title,
        }
        jsoned = json.dumps(data_to_json)
        return jsoned

    def change_status(self, status):
        self.status = status
        self.save()

    @classmethod
    def get_active_resource(cls):
        try:
            active_res = cls.objects.get(
                status__exact=DISPLAYING
            )
        except Resource.DoesNotExist:
            active_res = None
        return active_res

    @classmethod
    def get_pending_resources(cls, how_many=10):
        resources = cls.objects.filter(
            status__exact=WAITING
        ).order_by('id')[:how_many]
        return resources


