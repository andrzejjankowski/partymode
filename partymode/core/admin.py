# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from partymode.core.models import (
    User,
    Resource,
)


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(
        label=u'password',
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label='repeated password',
        widget=forms.PasswordInput,
    )

    class Meta:
        model = User

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(u'Passwords do not match.')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User

    def clean_password(self):
        return self.initial["password"]


class MyUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = (
        'email', 'firstname', 'surname', 'created', 'is_admin', 'is_active',
    )
    list_filter = ('is_admin', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (u'Personal info', {'fields': ('firstname', 'surname')}),
        (u'Permissions', {'fields': ('is_admin', 'is_active')}),
    )
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('email', 'password1', 'password2'),
            },
        ),
    )
    search_fields = ('email', 'firstname', 'surname')
    ordering = ('email', 'surname')
    filter_horizontal = ()


admin.site.register(User, MyUserAdmin)
admin.site.unregister(Group)


def register(model):
    def decorator(cls):
        admin.site.register(model, cls)
        return cls
    return decorator


@register(Resource)
class ResourceAdmin(admin.ModelAdmin):
    pass
