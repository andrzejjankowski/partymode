# -*- coding: utf-8 -*-

import requests

from bs4 import BeautifulSoup
from partymode.dispatcher.plugins.base import PluginBase

from partymode.core.models import Resource, IMAGE


class TheCodingLovePlugin(PluginBase):
    """ for thecodinglove.com """

    def process(self, url):
        img_url, title = None, None
        r  = requests.get(url)
        data = r.text
        tree = BeautifulSoup(data)
        for item in tree.findAll('p',{'class':'c1'}):
            img = item.find('img')
            if img:
                img_url = img['src']
            break
        for item in tree.findAll('h3'):
            title = item.text
        if img_url and title:
            resource = Resource()
            resource.resource_type = IMAGE
            resource.base_url = url
            resource.resource_title = title
            resource.resource_url = img_url
            resource.save()

    def can_run(self, url):
        if(
            url.startswith('http://thecodinglove.com') or
            url.startswith('thecodinglove.com')
        ):
            return True
        else:
            return False
