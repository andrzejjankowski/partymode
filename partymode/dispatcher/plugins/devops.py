# -*- coding: utf-8 -*-

import requests

from bs4 import BeautifulSoup
from partymode.dispatcher.plugins.base import PluginBase

from partymode.core.models import Resource, IMAGE


class DevOpsPlugin(PluginBase):
    """ for devopsreaction.com """

    def process(self, url):
        img_url, title = None, None
        r  = requests.get(url)
        data = r.text
        tree = BeautifulSoup(data)
        for item in tree.findAll('div',{'class':'item_content'}):
            img_url = item.find('img')
            title = item.find('a').text
        if img_url and title:
            resource = Resource()
            resource.resource_type = IMAGE
            resource.base_url = url
            resource.resource_title = title
            resource.resource_url = img_url['src']
            resource.save()

    def can_run(self, url):
        if(
            url.startswith('http://devopsreactions.tumblr.com') or 
            url.startswith('devopsreactions.tumblr.com')
        ):
            return True
        else:
            return False

