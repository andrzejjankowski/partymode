# -*- coding: utf-8 -*-

class PluginBase(object):
    def process(self, url):
        raise NotImplementedError()

    def can_run(self, url):
        raise NotImplementedError()
