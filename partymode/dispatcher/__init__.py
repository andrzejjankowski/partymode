# -*- coding: utf-8 -*-

import django_rq

from django.conf import settings

from partymode.core.models import Resource, IMAGE


def get_resource_processor(url):
    for module_path, class_name in settings.SUPPORTED_PLUGINS:
        plugin_module = __import__(module_path, fromlist=[class_name])
        plugin_instance = getattr(plugin_module, class_name)()
        if plugin_instance.can_run(url):
            return module_path, class_name


def run_resource_processor(url, plugin_module, plugin_class):
    queue = django_rq.get_queue(
        'processors' if 'processors' in settings.RQ_QUEUES else 'default',
    )
    job = queue.enqueue_call(
        func=_run_resource_processor,
        kwargs={
            'url': url,
            'plugin_module': plugin_module,
            'plugin_class': plugin_class,
        },
        timeout=120,
        result_ttl=0,
    )
    return job.id


def _run_resource_processor(url, plugin_module, plugin_class):
    module = __import__(plugin_module, fromlist=[plugin_class])
    plugin = getattr(module, plugin_class)()
    plugin.process(url)

