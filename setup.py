# -*- encoding: utf-8 -*-

import sys
from setuptools import setup, find_packages


assert sys.version_info >= (3, 2), "Python 3.2+ required."


from partymode import VERSION
version = ".".join(str(num) for num in VERSION)


setup(
    name='PartyMode',
    version=version,
    license='',
    author='',
    url='',
    author_email='',
    description='PartyMode',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Django==1.6.1',
        'South==0.8.3',
        'rq==0.3.11',
        'django_rq==0.6.0',
        'requests==2.1.0',
        'beautifulsoup4==4.3.2',
    ],
    entry_points={
        'console_scripts': [
            'partymode = partymode.__main__:main',
        ],
    },
)

